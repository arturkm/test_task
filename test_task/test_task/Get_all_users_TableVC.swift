//
//  TableTableViewController.swift
//  test_task
//
//  Created by artur on 4/27/19.
//  Copyright © 2019 arturkm. All rights reserved.
//

import UIKit
import GithubAPI
import SDWebImage

class Get_all_users_TableVC: UITableViewController {
    
    private var user_api = UserAPI()
    private var users_arr : [AllUsersResponse] = [AllUsersResponse]()
    private let refresh_control = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 44
        load_users_data()
        add_refresh_control()
        
        // Uncomment the following line to preserve selection between presentations
        //         self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc private func load_users_data() {
        _ = self.user_api.getAllUsers(since: "\(self.users_arr.last?.id ?? 0)", completion: { (response, error) in
            if let response = response {
                self.users_arr.append(contentsOf: response)
                DispatchQueue.main.async {
                    self.refresh_control.endRefreshing()
                    self.tableView.reloadData()
                }
            } else {
                print("error is : \(String(describing: error))")
            }
        })
    }
    private func add_refresh_control() {
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = refresh_control
        } else {
            tableView.addSubview(refresh_control)
        }
        refresh_control.addTarget(self, action: #selector(load_users_data), for: .valueChanged)
        refresh_control.attributedTitle = NSAttributedString(string: "load new users")
    }
}

// MARK: - Table view data source
extension Get_all_users_TableVC {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users_arr.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "github_user", for: indexPath)
        cell.textLabel?.text = users_arr[indexPath.row].login
        if let id = users_arr[indexPath.row].id {
            cell.detailTextLabel?.text = String(id)
        }
        cell.imageView?.sd_setImage(with: URL(string: users_arr[indexPath.row].avatarUrl!), placeholderImage: UIImage(named: "default"), options: [], completed: nil)
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == users_arr.count - 10 {
            load_users_data()
        }
    }
    
// MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "user_details") {
            let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!)
            let controller = segue.destination as! User_Details_VC
            controller.user_name = cell?.textLabel?.text
        }
    }
}
