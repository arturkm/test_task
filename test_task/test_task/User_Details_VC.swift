//
//  ViewController.swift
//  test_task
//
//  Created by artur on 4/27/19.
//  Copyright © 2019 arturkm. All rights reserved.
//

import UIKit
import GithubAPI
import SDWebImage

class User_Details_VC: UIViewController {
    
    @IBOutlet weak var ui_avatar: UIImageView!
    @IBOutlet weak var ui_user_name: UILabel!
    @IBOutlet weak var ui_user_email: UILabel!
    @IBOutlet weak var ui_user_org: UILabel!
    @IBOutlet weak var ui_following: UILabel!
    @IBOutlet weak var ui_followers: UILabel!
    @IBOutlet weak var ui_created: UILabel!
    
    private var user_api = UserAPI()
    var user_name : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup_ui()
        print(user_name)
        load_user_data()
    }
    
    private func setup_ui() {
        ui_avatar.clipsToBounds = true
        ui_avatar.layer.cornerRadius = 20
    }
    private func convert_date (_ string : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z"
        let date = dateFormatter.date(from: string)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    private func load_user_data() {
        _ = user_api.getUser(username: user_name) { (response, error) in
            if let response = response {
                DispatchQueue.main.async {
                    self.ui_avatar.sd_setImage(with: URL(string: response.avatarUrl!), completed: nil)
                    self.ui_user_name.text = response.name
                    if let email = response.email {
                        self.ui_user_email.text = email
                    } else {
                        self.ui_user_email.isHidden = true
                    }
                    self.ui_user_org.text = response.organizationsUrl
                    self.ui_followers.text = "Followers count : \(response.followers!)"
                    self.ui_following.text = "Following count : \(response.following!)"
                    self.ui_created.text = self.convert_date(response.createdAt!)
                }
            } else {
                print("error is : \(String(describing: error))")
            }
        }
    }
}
